//
//  Seal.h
//  PeevedPenguins
//
//  Created by Justin Christensen on 10/24/13.
//
//

#import "CCSprite.h"

@interface Seal : CCSprite{
    
}
@property int health;

-(id) initWithSealImage;

@end