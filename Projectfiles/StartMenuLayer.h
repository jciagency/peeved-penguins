//
//  Menu.h
//  PeevedPenguins
//
//  Created by Justin Christensen on 10/2/13.
//
// Following the tutorial on creating a new layer


@interface StartMenuLayer : CCLayer
{
    
}

+(StartMenuLayer*) sharedInstanceOfStartMenuLayer; //ADD THIS LINE
-(void) setUpMenu;
-(void) loadLevel: (CCMenuItem*)menuItem;
-(void) doNothing: (CCMenuItem*)menuItem;
@end

