/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "GameLayer.h"
#import "StartMenuLayer.h"
#import "EndGameLayer.h"
#import "SimpleAudioEngine.h"
#import "Seal.h"

const float PTM_RATIO = 32.0f;
#define FLOOR_HEIGHT    50.0f

CCSprite *projectile;
CCSprite *block;
CGRect firstrect;
CGRect secondrect;
NSMutableArray *blocks = [[NSMutableArray alloc] init];
NSInteger total_bullets = 0;
int max_bullets = 4;
int level_number;


@interface GameLayer (PrivateMethods)
-(void) enableBox2dDebugDrawing;
-(void) addSomeJoinedBodies:(CGPoint)pos;
-(void) addNewSpriteAt:(CGPoint)p;
-(b2Vec2) toMeters:(CGPoint)point;
-(CGPoint) toPixels:(b2Vec2)vec;
-(id) initWithNumber:(int)levelNumber;
@end

@implementation GameLayer


-(id) initWithNumber:(int)levelNumber
{
	if ((self = [super init]))
	{
		CCLOG(@"%@ init", NSStringFromClass([self class]));
        
        bullets = [[NSMutableArray alloc] init];
        
        // Construct a world object, which will hold and simulate the rigid bodies.
		b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
		world = new b2World(gravity);
		world->SetAllowSleeping(YES);
		//world->SetContinuousPhysics(YES);
        
        //create an object that will check for collisions
		contactListener = new ContactListener();
		world->SetContactListener(contactListener);
        
		glClearColor(0.1f, 0.0f, 0.2f, 1.0f);
        
        CGSize screenSize = [CCDirector sharedDirector].winSize;

        
        b2Vec2 lowerLeftCorner =b2Vec2(0,FLOOR_HEIGHT/PTM_RATIO);
		b2Vec2 lowerRightCorner = b2Vec2(2*screenSize.width/PTM_RATIO,FLOOR_HEIGHT/PTM_RATIO);
		b2Vec2 upperLeftCorner = b2Vec2(0,screenSize.height/PTM_RATIO);
		b2Vec2 upperRightCorner = b2Vec2(2*screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO);
		
		// Define the static container body, which will provide the collisions at screen borders.
		b2BodyDef screenBorderDef;
		screenBorderDef.position.Set(0, 0);
        screenBorderBody = world->CreateBody(&screenBorderDef);
		b2EdgeShape screenBorderShape;
        
        screenBorderShape.Set(lowerLeftCorner, lowerRightCorner);
        screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        screenBorderShape.Set(lowerRightCorner, upperRightCorner);
        screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        screenBorderShape.Set(upperRightCorner, upperLeftCorner);
        screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        screenBorderShape.Set(upperLeftCorner, lowerLeftCorner);
        screenBorderBody->CreateFixture(&screenBorderShape, 0);
        
        
        //Add all the sprites to the game, including blocks and the catapult. It's tedious...
        //See the storing game data tutorial to learn how to abstract all of this out to a plist file
        
        CCSprite *sprite = [CCSprite spriteWithFile:@"background.png"];
        sprite.anchorPoint = CGPointZero;
        [self addChild:sprite z:-1];
        
        sprite = [CCSprite spriteWithFile:@"catapult.png"];
        sprite.anchorPoint = CGPointZero;
        sprite.position = CGPointMake(135.0f, FLOOR_HEIGHT);
        [self addChild:sprite z:0];
        
        // Bear animation
        // load the plist
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bearframes.plist"];
        
        // Load the spritesheet
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"bearframes.png"];
        
        // Add the Bear
        [self addChild:spriteSheet];
        
        // Set up the animation
        tauntingFrames = [NSMutableArray array];
        
        for(int i = 1; i <= 7; ++i)
        {
            [tauntingFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"bear%d.png", i]]];
        }
        
        //Initialize the bear with the first frame you loaded from your spritesheet, bear1
        
        sprite = [CCSprite spriteWithSpriteFrameName:@"bear1.png"];
        sprite.anchorPoint = CGPointZero;
        sprite.position = CGPointMake(50.0f, FLOOR_HEIGHT);
        
        //Create an animation from the set of frames you created earlier
        
        CCAnimation *taunting = [CCAnimation animationWithFrames: tauntingFrames delay:0.5f];
        
        //Create an action with the animation that can then be assigned to a sprite
        
        taunt = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:taunting restoreOriginalFrame:NO]];

        //tell the bear to run the taunting action
        [sprite runAction:taunt];
        
        [self addChild:sprite z:0];
            
        sprite = [CCSprite spriteWithFile:@"ground.png"];
        sprite.anchorPoint = CGPointZero;
        [self addChild:sprite z:10];
        
        CCSprite *arm = [CCSprite spriteWithFile:@"catapultarm.png"];
        
        
        //arm.position = CGPointMake(230.0f, FLOOR_HEIGHT+130.0f);
        [self addChild:arm z:-1];
        // Add code for the Box2D physics to controll arm

        // Setting the properties of our definition
        b2BodyDef armBodyDef;
        armBodyDef.type = b2_dynamicBody;
		
        armBodyDef.linearDamping = 1;
        //linear damping affects how much the velocity of an object slows over time -
        //this is in addition to friction
        
        armBodyDef.angularDamping = 1;
        //causes rotations to slow down. A value of 0 means there is no slowdown
        
        armBodyDef.position.Set(240.0f/PTM_RATIO,(FLOOR_HEIGHT+141.0f)/PTM_RATIO);
        armBodyDef.userData = (__bridge void*)arm; //this tells the Box2D body which sprite to update.

        //create a body with the definition we just created
        armBody = world->CreateBody(&armBodyDef);
        //the -> is C++ syntax; it is like calling an object's methods (the CreateBody "method")
        
        //Create a fixture for the arm
        b2PolygonShape armBox;
        b2FixtureDef armBoxDef;
        armBoxDef.shape = &armBox; //geometric shape
        armBoxDef.density = 0.3F; //affects collision momentum and inertia
        armBox.SetAsBox(15.0f/PTM_RATIO, 140.0f/PTM_RATIO);
        //this is based on the dimensions of the arm which you can get from your image editing software of choice
        armFixture = armBody->CreateFixture(&armBoxDef);
        
        // Create a joint to fix the catapult to the floor.
        b2RevoluteJointDef armJointDef;
        armJointDef.Initialize(screenBorderBody, armBody, b2Vec2(230.0f/PTM_RATIO, (FLOOR_HEIGHT+50.0f)/PTM_RATIO));
        
        
        /*When creating the joint you have to specify 2 bodies and the hinge point. You might be thinking: “shouldn’t the catapult’s arm attach to the base?”. Well, in the real world, yes. But in Box2d not necessarily. You could do this but then you’d have to create another body for the base and add more complexity to the simulation.*/
        
        armJointDef.enableMotor = true; // the motor will fight against our motion, sort of like a spring
        armJointDef.motorSpeed  = -5; // this sets the motor to move the arm clockwise, so when you pull it back it springs forward
        armJointDef.maxMotorTorque = 350; //this limits the speed at which the catapult can move
        armJointDef.enableLimit = true;
        armJointDef.lowerAngle  = CC_DEGREES_TO_RADIANS(9);
        armJointDef.upperAngle  = CC_DEGREES_TO_RADIANS(75);//these limit the range of motion of the catapult
        armJoint = (b2RevoluteJoint*)world->CreateJoint(&armJointDef);
        
        
        //schedules a call to the update method every frame
		[self scheduleUpdate];
        
        level_number = levelNumber;
        
        [self performSelector:@selector(resetGame:) withObject:[NSNumber numberWithInt:levelNumber] afterDelay:0.5f];
        
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"explo2.wav"];
        
        //[self createTargets];
	}
    
	return self;
}

//Create the bullets, add them to the list of bullets so they can be referred to later
- (void)createBullets: (int) count
{
    
    currentBullet = 0;
    CGFloat pos = 52.0f;
    
    if (count > 0)
    {
        // delta is the spacing between penguins
        // 52 is the position o the screen where we want the penguins to start appearing
        // 165 is the position on the screen where we want the penguins to stop appearing
        // 25 is the size of the penguin
        CGFloat delta = (count > 1)?((165.0f - 52.0f - 25.0f) / (count - 1)):0.0f;
        
        bullets = [[NSMutableArray alloc] initWithCapacity:count];
        
        for (int i=0; i<count; i++, pos+=delta)
        {
            // Create the bullet
            CCSprite *sprite = [CCSprite spriteWithFile:@"flyingpenguin.png"]; //create bullet sprite
            [self addChild:sprite z:1];
            
            b2BodyDef bulletBodyDef;
            bulletBodyDef.type = b2_dynamicBody;
            bulletBodyDef.bullet = true;
            //this tells Box2D to check for collisions more often - sets "bullet" mode on
            bulletBodyDef.position.Set(pos/PTM_RATIO,(FLOOR_HEIGHT+15.0f)/PTM_RATIO);
            
            bulletBodyDef.userData = (__bridge void*)sprite;
            b2Body *bullet = world->CreateBody(&bulletBodyDef);
            bullet->SetActive(false); //an inactive body does not collide with other bodies
    
            b2CircleShape circle;
            circle.m_radius = 12.0/PTM_RATIO;
            //you can figure the dimensions out by looking at flyingpenguin.png in image editing software
    
            b2FixtureDef ballShapeDef;
            ballShapeDef.shape = &circle;
            ballShapeDef.density = 0.8f;
            ballShapeDef.restitution = 0.2f;
            //set the "bounciness" of a body (0 = no bounce, 1 = complete (elastic) bounce)
    
            ballShapeDef.friction = 0.99f;
            //try changing these and see what happens!
            bullet->CreateFixture(&ballShapeDef);
            
            [bullets addObject:[NSValue valueWithPointer:bullet]];
        }
    }
}
            

- (BOOL)attachBullet
{
    if (currentBullet < [bullets count])
    {
     
        bulletBody = (b2Body*)[[bullets objectAtIndex:currentBullet++] pointerValue]; //get next bullet in the list
        bulletBody->SetTransform(b2Vec2(240.0f/PTM_RATIO, (200.0f+FLOOR_HEIGHT)/PTM_RATIO), 0.0f);
        //SetTransform sets the position and rotation of the bulletBody; the syntax is SetTransform( (b2Vec2) position, (float) rotation)
        
        bulletBody->SetActive(true);
        
        b2WeldJointDef weldJointDef;
        weldJointDef.Initialize(bulletBody, armBody, b2Vec2(240.0f/PTM_RATIO,(200.0f+FLOOR_HEIGHT)/PTM_RATIO));
        //syntax is Initialize(bodyA, bodyB, anchor point) - the anchor point is the point of rotation
        
        weldJointDef.collideConnected = false; //collisions between bodies connected by the weld joint are disabled
        
        bulletJoint = (b2WeldJoint*)world->CreateJoint(&weldJointDef);
        return YES;
    }
    
    return NO;
}


-(void) dealloc
{
	delete world;
    
#ifndef KK_ARC_ENABLED
	[super dealloc];
#endif
}



-(void) update:(ccTime)delta
{
    //Check for inputs and create a bullet if there is a tap
    KKInput* input = [KKInput sharedInput];
    if(input.anyTouchBeganThisFrame)
    {
        CGPoint location = input.anyTouchLocation; //get the touch location
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        //convert the location to Box2D coordinates
        
        if (locationWorld.x < armBody->GetWorldCenter().x + 40.0/PTM_RATIO)
            //if we're touching the catapult area
        {
            b2MouseJointDef md;
            md.bodyA = screenBorderBody;
            md.bodyB = armBody; //the body that is moved
            md.target = locationWorld; //bodyB is "pulled" to the target
            md.maxForce = 2000; //affects the speed that the catapult arm follows your finger
            //we create a mouse joint that can pull the catapult
            mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
        }
    }
    
    else if(input.anyTouchEndedThisFrame) // if someone's finger lets go
    {
        if (armJoint->GetJointAngle() >= CC_DEGREES_TO_RADIANS(20))
        {
            releasingArm = YES;
        }
        
        if (mouseJoint != nil)
        {

            //destroying the mouse joint lets the catapult motor rotate it back to its original position
            world->DestroyJoint(mouseJoint);

            [self performSelector:@selector(resetBullet:) withObject:[NSNumber numberWithInt:level_number] afterDelay:3.0f];
            mouseJoint = nil;

            //this calls the resetBullet method after an initial delay
        }
        //[self createBullets:1];
    }
    
    else if(input.touchesAvailable) //if they are dragging the catapult back
    {
        if (mouseJoint == nil) return;
        CGPoint location = input.anyTouchLocation;
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        
        mouseJoint->SetTarget(locationWorld);
        //moves the mouseJoint target to the touch location, which in turn pulls the catapult arm
    }
    
    
    // Arm is being released and bullet attached
    if (releasingArm && bulletJoint)
    {
        // Check if the arm has reached the end so we can let the bullet go
        if (armJoint->GetJointAngle() <= CC_DEGREES_TO_RADIANS(10))
        {
            releasingArm = NO; //reset state of arm
            
            // Destroy joint so the bullet will be free
            world->DestroyJoint(bulletJoint);
            bulletJoint = nil;
            
        }
    }
    
    float timeStep = 0.03f;
    int32 velocityIterations = 8;
    int32 positionIterations = 1;
    world->Step(timeStep, velocityIterations, positionIterations);
    
    //Bullet is moving.
    if (bulletBody && bulletJoint == nil)
    {
        b2Vec2 position = bulletBody->GetPosition();
        CGPoint myPosition = self.position;
        CGSize screenSize = [CCDirector sharedDirector].winSize;
        
        // Move the camera.
        if (position.x > screenSize.width / 2.0f / PTM_RATIO)
            //if the bullet is past the edge of the screen
        {
            //self.position refers to the window's position - subtracting from self.position moves the screen to the right
            //meaning that the screen position is negative as it moves
            //only shift the screen a maximum of one screen size to the right
            myPosition.x = -MIN(screenSize.width * 2.0f - screenSize.width, position.x * PTM_RATIO - screenSize.width / 2.0f);
            self.position = myPosition;
            
        }
    }
    
    
 
    
    
    // Add update for the Box2D methods
    //get all the bodies in the world
    for (b2Body* body = world->GetBodyList(); body != nil; body = body->GetNext())
    {
        //get the sprite associated with the body
        CCSprite* sprite = (__bridge CCSprite*)body->GetUserData();
        if (sprite != NULL)
        {
            // update the sprite's position to where their physics bodies are
            sprite.position = [self toPixels:body->GetPosition()];
            float angle = body->GetAngle();
            sprite.rotation = CC_RADIANS_TO_DEGREES(angle) * -1;
        }
    }
    
    for (b2Body* body = world->GetBodyList(); body != nil; body = body->GetNext())
    {
        //get the sprite associated with the body
        CCSprite* sprite = (__bridge CCSprite*)body->GetUserData();
        if (sprite != NULL && sprite.tag==2)
        {
            if ([sprite isKindOfClass:[Seal class]])
            {
                if( ((Seal*)sprite).health==1 )
                {
                    [self removeChild:sprite cleanup:NO];
                    [enemies removeObject:[NSValue valueWithPointer:body]];
                    world->DestroyBody(body);
                    
                }
                else
                {
                    ((Seal*)sprite).health--;
                }
            }
            else
            {
                [self removeChild:sprite cleanup:NO];
                world->DestroyBody(body);
            }
        }
    }
    
}




-(void) endScene{
    [[CCDirector sharedDirector] replaceScene : (CCScene*)[[EndGameLayer alloc] init]];
}

// convenience method to convert a b2Vec2 to a CGPoint
-(CGPoint) toPixels:(b2Vec2)vec
{
	return ccpMult(CGPointMake(vec.x, vec.y), PTM_RATIO);
}


- (void)createTargets:(int) level_number
{
    targets = [[NSMutableSet alloc] init];
    enemies = [[NSMutableSet alloc] init];
    
    // Check which level to load
    NSString *levelstart;
    if (level_number == 1) {
        levelstart = @"level1Start";
    }
    else {
        levelstart = @"level2Start";
    }
    
    // Load in the plist file
    NSString *path = [[NSBundle mainBundle] pathForResource:levelstart ofType:@"plist"];
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:path];
    
    // blocks are the level blocks, each array is a dictionary with x,y,z,spriteName
    // Seals are the level seals, same structure as blocks
    
    NSArray *startBlocks = [level objectForKey:@"block"];
    NSArray *startSeals = [level objectForKey:@"seals"];
    
    // Want to find out how many block I have
    int lengthBlocks = [startBlocks count];
    int lengthSeals = [startSeals count];
    
    // iterate through each block
    for (int i = 0;i < lengthBlocks; i++){
    // Load the dictionary for each block
    NSDictionary *blk = [startBlocks objectAtIndex:i];
    // Place and add each item
    
    [self createTarget:[blk objectForKey:@"spriteName"] atPosition:CGPointMake([[blk objectForKey:@"x"] doubleValue],[[blk objectForKey:@"y"] doubleValue]) rotation:[[blk objectForKey:@"rotation"] doubleValue] isCircle:[[blk objectForKey:@"isCircle"] integerValue] isStatic:[[blk objectForKey:@"isStatic"] integerValue] isEnemy:[[blk objectForKey:@"isEnemy"] integerValue]];
    }
    
    // iterate through each seal
    for (int i = 0;i < lengthSeals; i++){
        // Load the dictionary for each block
        NSDictionary *sls = [startSeals objectAtIndex:i];
        // Place and add each item
        
        [self createTarget:[sls objectForKey:@"spriteName"] atPosition:CGPointMake([[sls objectForKey:@"x"] doubleValue],[[sls objectForKey:@"y"] doubleValue]) rotation:[[sls objectForKey:@"rotation"] doubleValue] isCircle:[[sls objectForKey:@"isCircle"] integerValue] isStatic:[[sls objectForKey:@"isStatic"] integerValue] isEnemy:[[sls objectForKey:@"isEnemy"] integerValue]];
    }
    
}


- (void)createTarget:(NSString*)imageName
          atPosition:(CGPoint)position
            rotation:(CGFloat)rotation
            isCircle:(BOOL)isCircle
            isStatic:(BOOL)isStatic
             isEnemy:(BOOL)isEnemy
{
    //seals are enemies, and since we create a custom Seal class,
    //we have to handle it differently
    CCSprite* sprite;
    if (isEnemy)
    {
        sprite = [[Seal alloc] initWithSealImage];
        [self addChild:sprite z:1];
    }
    else
    {
        sprite = [CCSprite spriteWithFile:imageName];
        [self addChild:sprite z:1];
    }
    
    b2BodyDef bodyDef;
    bodyDef.type = isStatic?b2_staticBody:b2_dynamicBody; //this is a shorthand/abbreviated if-statement
    bodyDef.position.Set((position.x+sprite.contentSize.width/2.0f)/PTM_RATIO,(position.y+sprite.contentSize.height/2.0f)/PTM_RATIO);
    bodyDef.angle = CC_DEGREES_TO_RADIANS(rotation);
    bodyDef.userData = (__bridge void*) sprite;
    b2Body *body = world->CreateBody(&bodyDef);
    
    b2FixtureDef boxDef;
    
    if (isCircle)
    {
        b2CircleShape circle;
        circle.m_radius = sprite.contentSize.width/2.0f/PTM_RATIO;
        boxDef.shape = &circle;
    }
    else
    {
        
        b2PolygonShape box;
        box.SetAsBox(sprite.contentSize.width/2.0f/PTM_RATIO,
                     sprite.contentSize.height/2.0f/PTM_RATIO);
        //contentSize is used to determine the dimensions of the sprite
        boxDef.shape = &box;
        
    }
    if (isEnemy)
        
    {
        boxDef.userData = (void*)1;
        [enemies addObject:[NSValue valueWithPointer:body]];
    }
    
    boxDef.density = 0.5f;
    body->CreateFixture(&boxDef);
    [targets addObject:[NSValue valueWithPointer:body]];
}

- (void)resetBullet:(NSNumber*)level_number
{
    if ([enemies count] == 0)
    {
        int level = [level_number intValue];
        if (level == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:level_number forKey:@"passLevel1"];
            [self endScene];
        }
        else{
            [self endScene];
        }
        
        // game over
        // We'll do something here later
    }
    else if ([self attachBullet])
    {
        [self runAction:[CCMoveTo actionWithDuration:2.0f position:CGPointZero]];
        
    }
    else
    {
        [self endScene];
        // We can reset the whole scene here
        // Also, let's do this later
    }
}

- (void)resetGame:(NSNumber*)level_number
{
    int level = [level_number intValue];
    [self createBullets:max_bullets]; //load 4 bullets
    [self attachBullet]; //attach the first bullet
    [self createTargets:level];
}








/*
-(void) setItems:(int)levelNumber{
    // define any used variables
    NSString *levelstart;
    if (levelNumber == 1) {
        levelstart = @"level1Start";
    }
    else {
        levelstart = @"level2Start";
    }
    
    // Load in the plist file
    NSString *path = [[NSBundle mainBundle] pathForResource:levelstart ofType:@"plist"];
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:path];
    // blocks are the level blocks, each array is a dictionary with x,y,z,spriteName
    // Seals are the level seals, same structure as blocks
    NSArray *startBlocks = [level objectForKey:@"block"];
    NSArray *startSeals = [level objectForKey:@"seals"];
    
    // Want to find out how many block I have
    int lengthBlocks = [startBlocks count];
    int lengthSeals = [startSeals count];
    
    // Ideal to use a for loop to set each block
    for (int i=0; i < lengthBlocks; i++) {
        // Load the dictionary for each block
        NSDictionary *blk = [startBlocks objectAtIndex:i];
        // Place and add each item
        CCSprite *sprite = [CCSprite spriteWithFile:[blk objectForKey:@"spriteName"]];
        sprite.position = CGPointMake([[blk objectForKey:@"x"] floatValue],[[blk objectForKey:@"y"] floatValue]);
        [blocks addObject:sprite];
        [self addChild:sprite z:[[blk objectForKey:@"z"] floatValue]];
    }
        for (int i=0; i < lengthSeals; i++) {
            // Load the dictionary for each seal
            NSDictionary *blk = [startSeals objectAtIndex:i];
            
            // Place and add each item
            Seal *seal = [[Seal alloc] initWithSealImage];
            seal.position = CGPointMake([[blk objectForKey:@"x"] floatValue],[[blk objectForKey:@"y"] floatValue]);
            [blocks addObject:seal];
            [self addChild:seal z:[[blk objectForKey:@"z"] floatValue]];
    }
     
}
*/

@end
