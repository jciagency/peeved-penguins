//
//  EndGameLayer.h
//  PeevedPenguins
//
//  Created by Justin Christensen on 10/9/13.
//
//

@interface EndGameLayer : CCLayer
{
NSString* endGameString;
NSString* endGameFontName;
int endGameFontSize;
}
@property (nonatomic, copy) NSString* endGameString;
@property (nonatomic, copy) NSString* endGameFontName;
@property (nonatomic) int endGameFontSize;

+(EndGameLayer*) sharedInstanceOfEndGameLayer; //ADD THIS LINE
-(void) setUpMenu;
-(void) returnHome: (CCMenuItem*)menuItem;
-(void) doNothing: (CCMenuItem*)menuItem;
-(void) resetScore: (CCMenuItem*)menuItem;
@end
