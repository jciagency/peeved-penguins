//
//  StartMenuLayer.cpp
//  PeevedPenguins
//
//  Created by Justin Christensen on 10/2/13.
//
//



#include "StartMenuLayer.h"
#include "GameLayer.h"
CCSprite * bg;


@interface StartMenuLayer (PrivateMethods)
@end



@implementation StartMenuLayer
static StartMenuLayer* instanceOfStartMenuLayer;

-(id) init
{
	if ((self = [super init]))
	{
        NSNumber *level1Complete = 0;
        [[NSUserDefaults standardUserDefaults] setObject:level1Complete forKey:@"level1Complete"];
        
        instanceOfStartMenuLayer = self;
        bg = [CCSprite spriteWithFile:@"menuBackground.png"];
        bg.position = ccp(240,160);
        [self addChild:bg];
        
        [self setUpMenus];
    

    
    
    }
    return self;
}

-(void) setUpMenus{
    // define variables
    CCMenuItemLabel * menuItem2;
    CGSize s =[[CCDirector sharedDirector]winSize];
    
    // Set up the two menu labels for levels 1 and 2 and a button to reset
    // the level 1 pass

    CCLabelTTF* label1 = [CCLabelTTF labelWithString:@"Level 1"
                                           fontName:@"Marker Felt"
                                           fontSize:25];
    label1.color = ccc3(255,127,0);

    CCLabelTTF* label2 = [CCLabelTTF labelWithString:@"Level 2"
                                            fontName:@"Marker Felt"
                                            fontSize:25];
    
    
    CCMenuItemLabel * menuItem1 = [CCMenuItemLabel itemWithLabel:label1
        target:self
        selector:@selector(loadLevel:)];
        //menuItem1.position = ccp(0,-s.height/3.3);
        menuItem1.tag = 1;
    
    
    // Check if level 1 finished and if so allow level 2
    NSNumber *level1Complete = [[NSUserDefaults standardUserDefaults] objectForKey:@"passLevel1"];
    int level1 = [level1Complete intValue];
    
    if (level1 == 0) {
        label2.color = ccc3(205,201,201);
        menuItem2 = [CCMenuItemLabel itemWithLabel:label2
        target:self
        selector:@selector(doNothing:)];
        //menuItem2.position = ccp(0,-s.height/3.3);
        menuItem2.tag = 2;
    }
    
    else {
        label2.color = ccc3(255,127,0);
        menuItem2 = [CCMenuItemLabel itemWithLabel:label2
        target:self
        selector:@selector(loadLevel:)];
        //menuItem2.position = ccp(0,-s.height/3.3);
        menuItem2.tag = 2;

        }
    
    
    CCMenu *myMenu = [CCMenu menuWithItems:menuItem1,menuItem2, nil];
    
    [myMenu alignItemsVertically];
    
    [self addChild:myMenu];
}


-(void) loadLevel: (CCMenuItem *) menuItem {
    
    [[CCDirector sharedDirector] replaceScene: (CCScene*)[[GameLayer alloc] initWithNumber:menuItem.tag]];
}

+(StartMenuLayer*) sharedInstanceOfStartMenuLayer
{
    NSAssert(instanceOfStartMenuLayer != nil, @"StartMenuLayer instance not yet initialized!");
    return instanceOfStartMenuLayer;
}

-(void) doNothing: (CCMenuItem*) menuItem{
    
    
}


@end