//
//  EndGameLayer.cpp
//  PeevedPenguins
//
//  Created by Justin Christensen on 10/9/13.
//
//

#include "EndGameLayer.h"
#include "GameLayer.h"
#include "StartMenuLayer.h"

@interface EndGameLayer (PrivateMethods)
@end



@implementation EndGameLayer

static EndGameLayer* instanceOfEndGameLayer;
@synthesize endGameString, endGameFontName;
@synthesize endGameFontSize;

-(id) init
{
	if ((self = [super init]))
	{
        instanceOfEndGameLayer = self;
        [self setUpMenus];
        
        
        
        
    }
    return self;
}

-(void) setUpMenus{
    //CGSize s =[[CCDirector sharedDirector]winSize];
    //CCDirector* director = [CCDirector sharedDirector];
    
    // get the hello world string from the config.lua file
    [KKConfig injectPropertiesFromKeyPath:@"EndGameSettings" target:self];
    
    CCLabelTTF* label = [CCLabelTTF labelWithString:endGameString
                                           fontName:endGameFontName
                                           fontSize:endGameFontSize];
    
    label.color = ccc3(255,127,0);
    
    CCLabelTTF* label2 = [CCLabelTTF labelWithString:@"Start Menu"
                                            fontName:@"Marker Felt"
                                            fontSize:25];
    label2.color = ccc3(255,127,0);
    
    CCLabelTTF* label3 = [CCLabelTTF labelWithString:@"Reset Score"
                                            fontName:@"Marker Felt"
                                            fontSize:25];
    label3.color = ccc3(255,127,0);
    
    
    

    CCMenuItemLabel * menuItem1 = [CCMenuItemLabel itemWithLabel:(label)
                    target:self
                    selector:@selector(doNothing:)];
    menuItem1.tag = 1;
    
    CCMenuItemLabel * menuItem2 = [CCMenuItemLabel itemWithLabel:(label2)
                                                          target:self
                                                        selector:@selector(returnHome:)];
    menuItem2.tag = 2;
    
    CCMenuItemLabel * menuItem3 = [CCMenuItemLabel itemWithLabel:(label3)
                                                          target:self
                                                        selector:@selector(resetScore:)];
    menuItem3.tag = 3;
    
    
    
    
    
    CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, menuItem2, menuItem3, nil];
    
    [myMenu alignItemsVertically];
    [self addChild:myMenu];
}


-(void) returnHome: (CCMenuItem *) menuItem {
    [[CCDirector sharedDirector] replaceScene: (CCScene*)[[StartMenuLayer alloc] init]];
    
}

-(void) doNothing: (CCMenuItem*) menuItem{
    
}

-(void) resetScore: (CCMenuItem*) menuItem{
    [[NSUserDefaults standardUserDefaults] setObject:0 forKey:@"passLevel1"];
    [[CCDirector sharedDirector] replaceScene: (CCScene*)[[StartMenuLayer alloc] init]];
    
}



+(EndGameLayer*) sharedInstanceOfEndGameLayer
{
    NSAssert(instanceOfEndGameLayer != nil, @"EndGameLayer instance not yet initialized!");
    return instanceOfEndGameLayer;
}



@end